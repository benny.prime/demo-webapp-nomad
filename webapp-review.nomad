job "demo-[[.environment_slug]]" {
  type = "service"
  datacenters = ["dc1"]
  meta {
    git_sha = "[[.git_sha]]"
  }
  group "demo" {
    count = 1

    network {
      mbits = 10
      port  "http"{}
    }

    task "server" {
      env {
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }

      driver = "exec"
   
      artifact {
        source = "[[.artifact_url]]"
      }

      config {
        command = "demo-webapp"
      }

      resources {
        cpu = 128
        memory = 128
      }

      service {
        name = "review-[[.environment_slug]]"
        port = "http"

        tags = [
          "traefik.enable=true",
          "traefik.tags=service",
          "traefik.http.routers.frontend_review.entrypoints=http",
          "traefik.http.routers.frontend_review.rule=Path(`/`)",
          "traefik.http.routers.frontend_review.rule=Host(`[[.deploy_url]]`)",
        //  "traefik.http.middlewares.frontend-https-redirect.redirectscheme.scheme=https",
        //  "traefik.http.middlewares.sslheader.headers.customrequestheaders.X-Forwarded-Proto-https",
        //  "traefik.http.routers.frontend_review.middlewares=frontend-https-redirect",
          "traefik.http.routers.frontend_review_https.entrypoints=https",
          "traefik.http.routers.frontend_review_https.rule=Path(`/`)",
          "traefik.http.routers.frontend_review_https.rule=Host(`[[.deploy_url]]`)",
          "traefik.http.routers.frontend_review_https.tls=true",
          "traefik.http.routers.frontend_review_https.tls.certresolver=route53",
          "traefik.http.routers.frontend_review_https.tls.domains[0].main=primesupermarket.com",
          "traefik.http.routers.frontend_review_https.tls.domains[0].sans=*.primesupermarket.com"
        ]

        check {
          type = "http"
          path = "/"
          interval = "5s"
          timeout = "1s"
        }
      }

    }
  }
}
