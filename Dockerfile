# this is used for gitlab runner
FROM ubuntu:22.04

# ADD file:faa01786a6e5d5cc8855c0af88b1d49ddee288ddd39f479eff4da038a8e88065 in / 
# CMD ["bash"]
LABEL maintainer=cybc16
LABEL vendor=cybc16
ENV LEVANT_VERSION=0.3.3
ENV NOMAD_VERSION=1.7.6
WORKDIR /usr/bin/

RUN buildDeps="bash wget unzip" \
    && set -x && apt-get update && apt-get install -y $buildDeps ca-certificates \
    && wget -O levant.zip https://releases.hashicorp.com/levant/${LEVANT_VERSION}/levant_${LEVANT_VERSION}_linux_amd64.zip && unzip levant.zip && rm levant.zip && chmod +x /usr/bin/levant \ 
    && wget -O nomad.zip https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip && unzip nomad.zip && rm nomad.zip && chmod +x /usr/bin/nomad && apt-get clean && echo "Build complete."

# CMD ["bash", "-c", "buildDeps='bash wget unzip'", 'set -x && apt-get update && apt-get install -y $buildDeps ca-certificates && wget -O levant.zip https://releases.hashicorp.com/levant/${LEVANT_VERSION}/levant_${LEVANT_VERSION}_linux_amd64.zip && unzip levant.zip && rm levant.zip && chmod +x /usr/bin/levant && wget -O nomad.zip https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip && unzip nomad.zip && rm nomad.zip && chmod +x /usr/bin/nomad && apt-get clean && echo "Build complete."']
