package main

import (
	"fmt"
	"net/http"
	"os"
)

var GitCommit string

func version() string {
	return "v0.1.12"
}

func versionHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `
	<html>
		<h3>IP: %s</h3>
		<h3>Git SHA: %s</h3>
		<h3>Version: %s</h3>
	</html>`, GetLocalIP(), GitCommit, version())
}

func GetLocalIP() string {
	hostname, err := os.Hostname()
	ip := os.Getenv("IP")
	port := os.Getenv("PORT")

	fmt.Print(err);

    return fmt.Sprintf("(%v) - %v:%v", hostname, ip, port)
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	ip := os.Getenv("IP")
	addr := fmt.Sprintf("%v:%v", ip, port)
	http.HandleFunc("/", versionHandler)
	http.ListenAndServe(addr, nil)
}
