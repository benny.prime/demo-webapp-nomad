#!/bin/bash

BUILD_JOB_ID=$(cat build_job_id)
echo "Build Job ID: ${BUILD_JOB_ID}"
ARTIFACT_URL="${CI_PROJECT_URL}/-/jobs/${BUILD_JOB_ID}/artifacts/download?archive=zip"
echo "Artifact URL: ${ARTIFACT_URL}"
CANARY_DEPLOY_URL="canary.primesupermarket.com"
echo "Canary Deploy URL: ${CANARY_DEPLOY_URL}"
DEPLOY_URL="demo.primesupermarket.com"
echo "Deploy URL: ${DEPLOY_URL}"
levant deploy \
  -address "http://49.128.40.249:4646" \
  -var git_sha="${CI_COMMIT_SHORT_SHA}" \
  -var artifact_url="${ARTIFACT_URL}" \
  -var canary_deploy_url="${CANARY_DEPLOY_URL}" \
  -var deploy_url="${DEPLOY_URL}" \
  webapp-prod.nomad
